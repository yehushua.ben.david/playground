import pygame as pg , sys ,time as tps , random as rnd
pg.init()
dsp = pg.display.set_mode((512*2,512))


def mandelbrot(x,y,d=255):
    c0=complex(x,y)
    c =0
    i = 0
    while i < d:
        i+=1
        if abs(c)>2:
            return (255*i)//d
        c=c*c+c0
    return 0

def updatefx(cx,cy,zoom=1,presi=10):
    fx,dx=cx-zoom*2,cx+zoom*2
    fy,dy=cy-zoom,cy+zoom
    i=0
    pos=[]
    for x in range(dsp.get_width()):
        for y in range(dsp.get_height()):
            pos+=[(x,y)]
    rnd.shuffle(pos)
    for x,y in pos:
            c= mandelbrot(fx+ x*(dx-fx)/dsp.get_width(),
                          fy + y * (dy - fy) / dsp.get_height(),presi)
            c=255-c
            pg.draw.line(dsp,(c,c,c),(x,y),(x,y))
            yield i
            i+=1

cx,cy=0,0
zz=1
presi=250
lcx,lcy,lzz = 0,0,0
while 1 :
    t = tps.time()
    lcx, lcy, lzz = cx, cy, zz
    presi +=1
    for i in updatefx(cx, cy,zz,presi):
        if not (i % 1000):
            pg.display.flip()
            for e in pg.event.get():
                if e.type == pg.MOUSEBUTTONDOWN:

                    dsp.fill((255, 255, 255))
                    if e.button == 4:
                        zz *= 0.8
                    if e.button == 5:
                        zz *= 1.2
                    if e.button == 1:
                        fx, dx = cx - zz * 2, cx + zz * 2
                        fy, dy = cy - zz, cy + zz
                        cx = fx + e.pos[0] * (dx - fx) / dsp.get_width()
                        cy = fy + e.pos[1] * (dy - fy) / dsp.get_height()

                if e.type == pg.QUIT:
                    sys.exit()
        if cx!=lcx or  cy!=lcy or  zz!=lzz:
            break

    print(round(tps.time() - t, 5))
