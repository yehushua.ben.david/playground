import pygame as pg , sys ,time as tps , random as rnd
import json , os
pg.init()
ww,wh=1920,1080
dsp = pg.display.set_mode((ww,wh),pg.FULLSCREEN)
tmp = pg.Surface((ww//2,wh))
box = pg.Surface((ww,wh),pg.SRCALPHA)
box.fill([0]*3)
dsp.fill([255] * 3)
i=0
pos = [0,0]
doangle =0
zz=0.3
if os.path.exists("greatpos.json"):
    greatpos=json.load(open("greatpos.json"))
    greatpos["active"]=0
else:
    greatpos={"p":0,"active":0,"pos":[]}
def saveGreatPos():
    json.dump(greatpos,open("greatpos.json","w"))
def addtoGreatPos(pos,angle,zoom):
    greatpos["pos"]+=[{"p":pos,"a":angle,"z":zoom}]
    saveGreatPos()
while 1:
    if doangle:
        i += doangle
        dsp.fill([255] * 3)
    if greatpos["active"]:
        allok = 0
        dsp.fill([255] * 3)
        newpos=[0,0]
        if abs(pos[0]-greatpos["pos"][greatpos["p"]]["p"][0])< 2 :
            newpos[0] = greatpos["pos"][greatpos["p"]]["p"][0]
            allok+=1
        else:
            newpos[0] = pos[0] + (greatpos["pos"][greatpos["p"]]["p"][0]-pos[0])//10

        if abs(pos[1]-greatpos["pos"][greatpos["p"]]["p"][1])< 2 :
            newpos[1] = greatpos["pos"][greatpos["p"]]["p"][1]
            allok += 1
        else:
            newpos[1] = pos[1] + (greatpos["pos"][greatpos["p"]]["p"][1]-pos[1])//10
        pos = newpos
        if abs(i - greatpos["pos"][greatpos["p"]]["a"]) < 2:
            i = greatpos["pos"][greatpos["p"]]["a"]
            allok += 1
        else:
            i = i + (greatpos["pos"][greatpos["p"]]["a"] - i) // 10
        if abs(zz-greatpos["pos"][greatpos["p"]]["z"])< 1 :
            zz = greatpos["pos"][greatpos["p"]]["z"]
            allok += 1
        else:
            zz = zz + (greatpos["pos"][greatpos["p"]]["z"]-zz)//100




    tmp.fill([255]*3)
    rbox = pg.transform.rotozoom(box, i, zz)
    tmp.blit(rbox, (pos[0] - rbox.get_width() // 2, pos[1] - rbox.get_height() // 2))
    dsp.blit(tmp,(0,0))
    tmp.fill([255]*3)
    rbox = pg.transform.rotozoom(box, -1*i, zz)
    tmp.blit(rbox, ((tmp.get_width() - pos[0]) - rbox.get_width() // 2, pos[1] - rbox.get_height() // 2))
    dsp.blit(tmp, (ww//2, 0))
    box.fill([255] * 3)
    box.blit(dsp,(0,0),None, pg.BLEND_RGB_SUB)
    #box.fill([0]*3)
    pg.display.update()
    for e in pg.event.get():
        if e.type == pg.KEYDOWN:
            if e.key == pg.K_ESCAPE:
                sys.exit()
            if e.key == ord("s"):
                addtoGreatPos(pos,i,zz)
            if e.key == ord("b"):
                greatpos["active"]=0
            if e.key == ord("n"):
                if len(greatpos["pos"]):
                    greatpos["p"]=(greatpos["p"]+1) % len(greatpos["pos"])
                    greatpos["active"]=1


        if e.type == pg.MOUSEMOTION:
            pos = e.pos
        if e.type==pg.MOUSEBUTTONDOWN:
            if e.button == 4:
                zz+=0.015
            if e.button == 5:
                zz-=0.015
            if e.button == 3:
                doangle = 1
            if e.button == 1:
                doangle=-1
        if e.type==pg.MOUSEBUTTONUP:
            doangle=0
        if e.type==pg.QUIT:
            sys.exit()
