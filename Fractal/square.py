


import pygame as pg
pg.init()
dd=pg.display.set_mode((512*2,512))
def getsquare(p1,p2):
    x1 = p1[0]
    y1 = p1[1]
    x2 = p2[0]
    y2 = p2[1]
    xc = (x1 + x2) / 2
    yc = (y1 + y2) / 2
    xd = (x1 - x2) / 2
    yd = (y1 - y2) / 2

    x3 = xc - yd
    y3 = yc + xd
    x4 = xc + yd
    y4 = yc - xd
    return (x3,y3),(x4,y4)

done = 0
p1 = None
p2 = None

def recp(life,p1,p2):
    if life <=0:
        pg.draw.line(dd, [255] * 3, p1, p2)
    else:
        p3, p4 = getsquare(p1, p2)
        recp(life-1,p1,p3)
        recp(life-1,p3,p2)
        #    recp(life-1,p1,p4)
        #    recp(life-1,p4,p2)
lf=5
while not done :
    if p1 != None and p2 != None:
        dd.fill([0]*3)
        recp(lf,p1,p2)
        pg.display.flip()

    for e in pg.event.get():
        done = done or e.type==pg.QUIT
        if e.type == pg.MOUSEBUTTONDOWN:
            if e.button==1:
                p1=e.pos
            if e.button==4:
                lf+=1
                lf=min(lf,14)

            if e.button==5:
                lf-=1
                lf=max(lf,0)
        if e.type == pg.MOUSEMOTION:
            p2 = e.pos

pg.quit()