import pygame as pg , sys
pg.init()
ww,wh=512*2,512
dsp = pg.display.set_mode((ww,wh))
tmp = pg.Surface((ww//2,wh))
box = pg.Surface((ww,wh),pg.SRCALPHA)
angle=0
pos = [0,0]
addangle =0
zz=0.3
while 1:
    if addangle:
        angle += addangle
    tmp.fill([255]*3)
    rbox = pg.transform.rotozoom(box, angle, zz)
    tmp.blit(rbox, (pos[0] - rbox.get_width() // 2, pos[1] - rbox.get_height() // 2))
    dsp.blit(tmp,(0,0))
    tmp = pg.transform.flip(tmp,1,0)
    dsp.blit(tmp, (ww//2, 0))
    box.fill([255,255,255] )
    box.blit(dsp,(0,0),None, pg.BLEND_RGB_SUB)
    #box.fill([0]*3)
    pg.display.update()
    for e in pg.event.get():
        if e.type == pg.MOUSEMOTION:
            pos = e.pos
        if e.type==pg.MOUSEBUTTONDOWN:
            if e.button == 4:
                zz+=0.015
            if e.button == 5:
                zz-=0.015
            if e.button == 3:
                addangle = 1
            if e.button == 1:
                addangle=-1
        if e.type==pg.MOUSEBUTTONUP:
            addangle=0
        if e.type==pg.QUIT:
            sys.exit()