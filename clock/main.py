import pygame as pg , time as tps
pg.init()
ww,wh = 512,512
dd = pg.display.set_mode((ww,wh))

cadran = pg.transform.smoothscale(pg.image.load("cadran.png"),(ww,wh))
glass  = pg.transform.smoothscale(pg.image.load("glass.png"),(ww,wh))
hour = pg.transform.smoothscale(pg.image.load("heure.png"),(ww,wh))
minute = pg.transform.smoothscale(pg.image.load("minute.png"),(ww,wh))
tro = pg.transform.smoothscale(pg.image.load("troteuse.png"),(ww,wh))
done = 0
t=0
i=0
while not done:
    for e in pg.event.get():
        done = done | e.type == pg.QUIT

    if int(t)==int(tps.time()):
        continue
    t = tps.time()

    dd.blit(pg.transform.rotozoom(cadran,0,1),(0,0))
    r=pg.transform.rotate(hour, 360 -360  * ((2+(t / 60 / 60) ) % 12)/12   )

    dd.blit(r,((dd.get_width()/2)-(r.get_width()/2),(dd.get_height()/2)-(r.get_height()/2)))

    r = pg.transform.rotate(minute, 360 - 360 * ( int(t / 60 ) %60) / 60)
    dd.blit(r, ((dd.get_width() / 2) - (r.get_width() / 2), (dd.get_height() / 2) - (r.get_height() / 2)))

    r=pg.transform.rotate(tro,  360 - 360*(t % 60 ) /60    )
    dd.blit(r,((dd.get_width()/2)-(r.get_width()/2),(dd.get_height()/2)-(r.get_height()/2)))
    r = pg.transform.rotate(glass,  i )

    dd.blit(r, ((dd.get_width() / 2) - (r.get_width() / 2), (dd.get_height() / 2) - (r.get_height() / 2)))
    pg.display.flip()

pg.quit()

